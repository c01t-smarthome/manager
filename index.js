'use strict';

const server = require('./dist/main').default;

server.start(function startCb(err) {
  if (err) {
    process.exit(1);
  }
});

// PM2 sends IPC message for graceful shutdown
process.on('message', function msgCb(msg) {
  if (msg === 'shutdown') {
    server.stop();
  }
});

process.on('uncaughtException', (err) => {
  console.error('There was an uncaught error', err)
  setTimeout(() => {
    process.exit(1) //mandatory (as per the Node docs)
  }, 3000);
})

// Ctrl+c or kill $pid
process.on('SIGINT', () => server.stop());
process.on('SIGTERM', () => server.stop());
