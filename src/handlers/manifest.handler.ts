import {IMessageHandler} from './index';
import {inject, injectable} from 'inversify';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../services/logger';
import {ICache, ICACHE} from '../services/redis';
import {IEntityService, IUnitOfWork, Symbols} from '@viatsyshyn/ts-entity';
import {AttributeModel, DeviceModel, IAttribute, IWidget, UQ_DEVICE_TAG} from '../models';
import {IManifest} from '../models/manifest';
import {IStorage, ISTORAGE} from '../services/elastic';

@injectable()
export class ManifestHandler implements IMessageHandler {

  private readonly logger: ILogger;

  @inject(ICACHE)
  private cache!: ICache;

  @inject(ISTORAGE)
  private storage!: IStorage;

  @inject(Symbols.UnitOfWork)
  private unitOfWork!: IUnitOfWork;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService!: IEntityService<AttributeModel>;

  constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    this.logger = loggerFactory('manifest-handler');
  }

  async handle(manifest: IManifest, topic: string): Promise<void> {
    this.logger.log(topic, manifest);

    let [tag] = topic.split('/');
    let [
      device,
    ] = await Promise.all([
      this.deviceService.getOne({tag}),
      this.cache.set(tag, 'last-updated', new Date().getTime())
    ]);

    const processSingle = (prop: IAttribute | string): IAttribute => {
      return typeof prop === 'string' ? manifest.attributes.out.find(({name: _}) => _ === prop) : prop;
    };

    const processWidget = (widget: IWidget): IWidget => ({
      prime: widget.prime ? processSingle(widget.prime) : undefined,
      secondary: widget.secondary ? processSingle(widget.secondary) : undefined,
      state: widget.state ? processSingle(widget.state) : undefined,
      action: widget.action,
    });

    if (!device || device.firmware !== String(manifest.firmware)) {
      device = {
        zone: null,
        tag,
        name: manifest.device,
        description: manifest.description,
        ...device,
        firmware: String(manifest.firmware),
        icons: manifest.icons,
        widgets: {
          icon: processWidget(manifest.widgets.icon),
          device: manifest.widgets.device.map(processWidget)
        },
        forgotten: false,
      };

      await this.storage.create(device.tag);

      await this.unitOfWork.begin();
      try {
        device = await this.deviceService.upsertOne(device, UQ_DEVICE_TAG);

        const newAttributes = [].concat(
          processAttributes(manifest.attributes.in, 'in', device.id),
          processAttributes(manifest.attributes.out, 'out', device.id),
        );

        const attributes = await this.attributeService.get({device: device.id});

        const updatedAttributes: AttributeModel[] = newAttributes
          .map(({id, title, ...attribute}) => ({
            ...attributes.find(({key}) => key === attribute.key),
            ...attribute,
            title: attribute.definition.title || title || attribute.key,
          }));

        await this.attributeService.delete({device: device.id});
        await this.attributeService.add(updatedAttributes);

        await this.unitOfWork.commit();
      } catch (e) {
        await this.unitOfWork.rollback();

        throw e;
      }
    }
  }
}

function processAttributes(attributes: IAttribute[], kind: 'in' | 'out', device: number): Partial<AttributeModel>[] {
  return attributes.map(definition => ({
    definition,
    key: definition.name,
    kind,
    device,
  }))
}
