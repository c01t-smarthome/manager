import {IMessageHandler} from './index';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../services/logger';
import {inject, injectable} from 'inversify';
import {IPubSub, IPUBSUB} from '../services/mqtt';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import {AttributeModel, DeviceModel} from '../models';
import {IEvaluator, IEVALUATOR} from '../services/evaluator';

@injectable()
export class DesiredHandler implements IMessageHandler {

  private readonly logger: ILogger;

  @inject(IPUBSUB)
  private pubsub!: IPubSub;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService!: IEntityService<AttributeModel>;

  @inject(IEVALUATOR)
  private evaluator!: IEvaluator;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    this.logger = loggerFactory('desired-handler');
  }

  public async handle(msg: any, topic: string): Promise<void> {
    this.logger.log(topic, msg);

    let [tag, , key] = topic.split('/');

    const device = await this.deviceService.getOne({tag});
    const attribute = await this.attributeService.getOne({device: device.id, key, kind: 'in'});
    const {definition = null} = attribute || {};
    const {lambda = null, virtual = false} = definition || {};

    if (lambda) {
      await this.evaluator.eval(tag, lambda, msg);
    }

    if (virtual) {
      this.pubsub.pub(`${tag}/reported/${key}`, msg);
    }
  }
}
