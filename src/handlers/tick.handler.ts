import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../services/logger';
import {inject, injectable} from 'inversify';
import {ICache, ICACHE} from '../services/redis';
import {IPubSub, IPUBSUB} from '../services/mqtt';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import {AttributeModel, DeviceModel} from '../models';
const parser = require('cron-parser');

export interface ITickHandler {
  handle(thisTick: number, lastTick: number): Promise<void>;
}

@injectable()
export class TickHandler implements ITickHandler {
  private readonly logger: ILogger;

  @inject(ICACHE)
  private cache!: ICache;

  @inject(IPUBSUB)
  private pubsub!: IPubSub;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService!: IEntityService<AttributeModel>;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    this.logger = loggerFactory('tick-handler');
  }

  public async handle(thisTick: number, lastTick: number): Promise<void> {
    const cronOptions = {
      currentDate: new Date(thisTick),
      tz: 'Europe/Kiev'
    };

    let queue: {
      timestamp: number;
      device: number;
      key: string;
      value: any;
    }[] = [];

    const attributes = await this.attributeService.get({kind: 'in'});

    if (attributes.length < 1) {
      return;
    }

    for (const {setting, id, device, key} of attributes) {
      if (setting && setting.kind === 'schedule') {
        const {schedule} = setting;
        for (const timer in schedule) {
          if (schedule.hasOwnProperty(timer)) {
            const interval = parser.parseExpression(timer, cronOptions);
            const value = schedule[timer];

            queue.push({
              timestamp: interval.prev().getTime(),
              device,
              key,
              value,
            });
          }
        }
      }
    }

    queue = queue
      .filter(x => x.timestamp >= lastTick)
      .sort((_1, _2) => _1.timestamp - _2.timestamp);

    if (queue.length < 1) {
      return ;
    }

    this.logger.info(`processing ${queue.length} events since ${new Date(lastTick)}`);

    const ids = queue.map(({device: d}) => d);
    if (ids.length < 1) {
      return;
    }

    const devices = await this.deviceService.get(({id}) => id.in(ids));

    queue
      .forEach(({key, device, value}) => {
        const tag = devices.find(({id}) => id === device)!;
        this.pubsub.pub(`${tag}/desired/${key}`, value);
      });
  }
}
