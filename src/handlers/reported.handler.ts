import {IMessageHandler} from './index';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../services/logger';
import {inject, injectable} from 'inversify';
import {ICache, ICACHE} from '../services/redis';
import {IEvaluator, IEVALUATOR} from '../services/evaluator';
import {IPubSub, IPUBSUB} from '../services/mqtt';
import {IStorage, ISTORAGE} from '../services/elastic';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import {AttributeModel, DeviceModel, ZoneModel} from '../models';

@injectable()
export class ReportedHandler implements IMessageHandler {

  private readonly logger: ILogger;

  @inject(ICACHE)
  private cache!: ICache;

  @inject(IPUBSUB)
  private pubsub!: IPubSub;

  @inject(ISTORAGE)
  private storage!: IStorage;

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService!: IEntityService<ZoneModel>;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService!: IEntityService<AttributeModel>;

  @inject(IEVALUATOR)
  private evaluator!: IEvaluator;

  constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    this.logger = loggerFactory('reported-handler');
  }

  async handle(msg: any, topic: string): Promise<void> {
    this.logger.log(topic, msg);

    const now = new Date().getTime();

    let [tag, state, key] = topic.split('/');
    const [
      device,
    ] = await Promise.all([
      this.deviceService.getOne({tag}),
      this.cache.set(tag, 'last-updated', now)
    ]);

    const [
      attribute,
      zone,
    ] = await Promise.all([
      this.attributeService.getOne({key, device: device.id, kind: 'out'}),
      this.zoneService.getOne({id: device.zone}),
      this.cache.set(tag, key, msg),
    ]);

    await this.storage.index({
      timestamp: now,
      device: tag,
      state,
      topic,
      property: key,
      message: msg
    });

    const {definition, setting} = attribute;

    if (zone && setting && setting.kind === 'republish') {
      this.logger.log(`Processing device "${tag}::${key}" republish`);
      this.pubsub.pub(`${zone.uid}/updated/${setting.attribute}`, msg);
    }

    const {lambda = null} = definition || {};
    if (lambda) {
      await this.evaluator.eval(tag, lambda, msg);
    }
  }

}
