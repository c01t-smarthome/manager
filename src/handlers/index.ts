export interface IMessageHandler {
  handle(msg: any, topic: string): Promise<void>;
}

export * from './manifest.handler';
export * from './reported.handler';
export * from './desired.handler';
export * from './updated.handler';

export * from './tick.handler';
