import {IMessageHandler} from './index';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../services/logger';
import {inject, injectable} from 'inversify';
import {IPubSub, IPUBSUB} from '../services/mqtt';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import {AttributeModel, DeviceModel, ZoneModel} from '../models';
import {and} from '@viatsyshyn/ts-orm';

@injectable()
export class UpdatedHandler implements IMessageHandler {

  private readonly logger: ILogger;

  @inject(IPUBSUB)
  private pubsub!: IPubSub;

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService!: IEntityService<ZoneModel>;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService!: IEntityService<AttributeModel>;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    this.logger = loggerFactory('updated-handler');
  }

  public async handle(msg: any, topic: string): Promise<void> {
    let [uid, , key] = topic.split('/');

    const zone = await this.zoneService.getOne({uid});

    this.logger.log(`Processing zone "${zone.tag}::${key}" listeners`);

    const devices = await this.deviceService.get({zone: zone.id});
    const ids = devices.map(({id}) => id);
    const attributes = await this.attributeService.get(({device, kind}) => and(device.in(ids), kind.eq('in')));

    for (const {setting, device, key: target} of attributes) {
      if (setting && setting.kind === 'listen' && setting.attribute === key) {
        const {tag} = devices.find(({id}) => id === device)!;
        this.pubsub.pub(`${tag}/desired/${target}`, msg);
      }
    }
  }
}
