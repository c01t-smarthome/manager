import {IAttribute, IWidget} from '../../db';


export interface Manifest {
  version: 1 | '1' | '1.0';
  device: string;
  description: string;
  firmware: string;
  icons: { [key: string]: string };
  attributes: {
    idlePeriodSec?: number;
    out: IAttribute[];
    in: IAttribute[];
  };
  widgets: {
    icon: IWidget;
    device: IWidget[];
  };
}
