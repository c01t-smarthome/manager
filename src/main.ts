import 'reflect-metadata';

import {setUpRootContainer} from './ioc-container';
import {IDeviceManager} from './device-manager';

const container = setUpRootContainer();

export default container.get<IDeviceManager>(Symbol.for('IDeviceManager'));
