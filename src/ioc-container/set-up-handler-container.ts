import { Container } from 'inversify';

import {ISqlQuery} from '@viatsyshyn/ts-orm-pg';
import {IBuildableQueryCompiler, IDbDataReader} from '@viatsyshyn/ts-orm';
import {
  EntityService,
  IEntityService,
  IQueryExecutorProvider,
  IUnitOfWork,
  QueryExecutorProvider,
  Symbols,
  UnitOfWork
} from '@viatsyshyn/ts-entity';

import {UserFCMModel, UserModel, ZoneModel, DeviceModel, AttributeModel} from '../models';
import {DesiredHandler, IMessageHandler, ManifestHandler, ReportedHandler, UpdatedHandler} from '../handlers';
import {ITickHandler, TickHandler} from '../handlers';
import {Evaluator, IEVALUATOR, IEvaluator} from '../services/evaluator';

function bindServices(container: Container, ...entityClasses: Array<InstanceType<any>>): void {

  // bind entityServices
  entityClasses.forEach(Model => {
    const executorProvider = container.get<IQueryExecutorProvider<ISqlQuery>>(Symbols.QueryExecutorProvider);
    const queryBuilder = container.get<IBuildableQueryCompiler<ISqlQuery>>(Symbols.ISqlQueryCompiler);
    const dbDataRead = container.get<IDbDataReader<any>>(Symbols.dbReaderFor(Model));
    container
      .bind<IEntityService<any>>(Symbols.serviceFor(Model))
      .toConstantValue(new EntityService<any, ISqlQuery>(Model, executorProvider, queryBuilder, dbDataRead));
  });
}


export function setUpHandlerContainer(container: Container) {

  container.bind<Container>(Symbol.for('Container')).toConstantValue(container);

  container.bind<IUnitOfWork>(Symbols.UnitOfWork).to(UnitOfWork).inSingletonScope();
  container.bind<IQueryExecutorProvider<ISqlQuery>>(Symbols.QueryExecutorProvider).to(QueryExecutorProvider);

  bindServices(container,
    UserModel,
    UserFCMModel,
    ZoneModel,
    DeviceModel,
    AttributeModel,
  );

  // BIND EVALUATOR
  container.bind<IEvaluator>(IEVALUATOR).to(Evaluator);

  container.bind<IMessageHandler>(Symbol.for('ManifestHandler')).to(ManifestHandler);
  container.bind<IMessageHandler>(Symbol.for('ReportedHandler')).to(ReportedHandler);
  container.bind<IMessageHandler>(Symbol.for('DesiredHandler')).to(DesiredHandler);
  container.bind<IMessageHandler>(Symbol.for('UpdatedHandler')).to(UpdatedHandler);

  container.bind<ITickHandler>(Symbol.for('TickHandler')).to(TickHandler);
}
