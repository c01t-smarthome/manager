import { Container } from 'inversify';

// SERVICES
import {ILOGGERFACTORY, ILoggerFactory, LoggerFactory} from '../services/logger';

import * as config from 'config';

import {Pool, PoolConfig} from 'pg';
import {ISqlQuery, PgCompiler, PgDriver} from '@viatsyshyn/ts-orm-pg';
import {DbReader, IBuildableQueryCompiler, IDbDataReader, ISqlDataDriver} from '@viatsyshyn/ts-orm';
import {Symbols} from '@viatsyshyn/ts-entity';
import {Validator} from '@viatsyshyn/ts-validate';

import {IMQTTMANAGER, IMqttManager, IPUBSUB, IPubSub, MqttManager, PubSub} from '../services/mqtt';
import {Cache, ICACHE, ICache, IREDISMANAGER, IRedisManager, RedisManager} from '../services/redis';
import {ElasticManager, IELASTICMANAGER, IElasticManager, ISTORAGE, IStorage, Storage} from '../services/elastic';
import {IValidator, ModelValidator, register } from '../services/validation';

import {AttributeModel, DeviceModel, UserFCMModel, UserModel, ZoneModel} from '../models';
import {DeviceManager, IDeviceManager} from '../device-manager';

export function setUpRootContainer(): Container {

  const container = new Container();

  container.bind<Container>(Symbol.for('Container')).toConstantValue(container);

  container.bind<config.IConfig>(Symbol.for('IConfig')).toConstantValue(config);
  // BIND LOGGER
  container.bind<ILoggerFactory>(ILOGGERFACTORY).toConstantValue(LoggerFactory);

  // BIND DB
  const sqlConfig = config.get<PoolConfig>('dbConfig');
  const pool = new Pool(sqlConfig);
  container.bind<ISqlDataDriver<ISqlQuery>>(Symbols.ISqlDataDriver).toConstantValue(new PgDriver(pool));
  container.bind<IBuildableQueryCompiler<ISqlQuery>>(Symbols.ISqlQueryCompiler).toConstantValue(new PgCompiler());

  // BIND MANAGERS
  container.bind<IMqttManager>(IMQTTMANAGER).to(MqttManager).inSingletonScope();
  container.bind<IRedisManager>(IREDISMANAGER).to(RedisManager).inSingletonScope();
  container.bind<IElasticManager>(IELASTICMANAGER).to(ElasticManager).inSingletonScope();

  [
    UserModel,
    UserFCMModel,
    ZoneModel,
    DeviceModel,
    AttributeModel,
  ].forEach(Model => {
    container.bind<IDbDataReader<any>>(Symbols.dbReaderFor(Model))
      .toConstantValue(new DbReader<any>(Model));
  });

  register(container); // register constraints;
  [
  ].forEach(Model => {
    container.bind<IValidator<any>>(Validator)
      .toConstantValue(new ModelValidator<any>(Model, container))
      .whenTargetNamed(Model.name);
  });

  container.bind<IStorage>(ISTORAGE).to(Storage).inSingletonScope();
  container.bind<ICache>(ICACHE).to(Cache).inSingletonScope();
  container.bind<IPubSub>(IPUBSUB).to(PubSub).inSingletonScope();

  container.bind<IDeviceManager>(Symbol.for('IDeviceManager')).to(DeviceManager).inSingletonScope();

  return container;
}
