import {ConnectionString} from 'connection-string';

export function parseConnection(uri: string): {host?: string, port?: number, user?: string, password?: string} {
  const a = new ConnectionString(uri);
  const [host = {}] = a.hosts || [];
  return {
    host: host.name,
    port: host.port,
    user: a.user,
    password: a.password,
  }
}
