import { injectable, inject } from 'inversify';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../logger';

import {IElasticManager, IELASTICMANAGER} from './manager';

export interface IEvent<T = any> {
  device: string;
  state: string;
  topic: string;
  property: string;
  timestamp: number;
  message: T;
}

export interface ITimestamped {
  timestamp: number;
}

export interface IStorage {
  create(index: string): Promise<void>;
  index<T = any>(event: IEvent<T>): Promise<void>;
  query<T extends ITimestamped>(
    device: string,
    properties: Array<string | [string, string]>,
    since: Date,
    till: Date,
    interval: string,
  ): Promise<T[]>;
}

export const ISTORAGE = Symbol.for('IStorage');

@injectable()
export class Storage implements IStorage {
  private logger: ILogger;

  constructor(
    @inject(ILOGGERFACTORY) factory: ILoggerFactory,
    @inject(IELASTICMANAGER) private manager: IElasticManager,
  ) {
    this.logger = factory('storage');
  }

  async create(index: string): Promise<void> {
    const exists = await this.manager.client.indices.exists({index});
    if (!exists) {
      await this.manager.client.indices.create({index});
    }
  }

  async index<T = any>(event: IEvent<T>): Promise<void> {
    if (event.state !== 'reported') {
      return;
    }

    this.logger.log(`Log ${JSON.stringify(event)}`);

    const value = {
      index: event.device,
      type: event.device,
      body: {
        timestamp: new Date(event.timestamp).toISOString(),
        [event.property]: event.message
      },
    };
    await this.manager.client.index(value);
  }

  async query<T extends ITimestamped>(
    device: string,
    properties: Array<string | [string, string]>,
    since: Date,
    till: Date,
    interval: string,
  ): Promise<T[]> {

    this.logger.log('query', device, properties, since, till, interval);

    const {
      aggregations
    } = await this.manager.client
      .search({
        index: device,
        type: device,
        size: 0,
        body: {
          aggs : {
            report : {
              date_histogram : {
                field: 'timestamp',
                keyed: true,
                interval,
              },
              aggs: properties
                .map(field => typeof field === 'string' ? [field, 'avg'] : field)
                .map(([field, op]) => ({
                  [field]: {
                    [op]: {
                      field
                    }
                  }
                }))
                .reduce((p, c) => ({...p, ...c}), {}),

            }
          }
        }
      });

    const {report} = aggregations;
    const {buckets} = report;
    return Object.keys(buckets).map((key) => ({
      timestamp: buckets[key].key,
      ...(
        properties
          .map(field => typeof field === 'string' ? [field] : field)
          .map(([field]) => ({
            [field]: buckets[key][field].value,
          }))
          .reduce((p, c) => ({...p, ...c}), {})
      )
    })) as any;
  }
}
