import {connect as MqttConnect, MqttClient} from 'mqtt';
import {inject, injectable} from 'inversify';
import {ILogger, ILOGGERFACTORY, ILoggerFactory} from '../logger';
import {IConfig} from 'config';
import {parseConnection} from '../../utils';

export const IMQTTMANAGER = Symbol.for('IMqttManager');

export interface IMqttManager {
  readonly client: MqttClient;

  connect(): Promise<void>;
  disconnect(): Promise<void>;
}

@injectable()
export class MqttManager implements IMqttManager {
  get client(): MqttClient {
    if (!this.instance) {
      throw new Error('Mqtt not ready, call connect() first');
    }
    return this.instance!;
  }

  private instance: MqttClient | null = null;
  private logger: ILogger;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
    @inject(Symbol.for('IConfig')) private config: IConfig,
  ) {
    this.logger = loggerFactory('mqtt');
  }

  connect(): Promise<void> {
    this.logger.info(`Start Mosquitto connection`);
    return new Promise<void>((resolve) => {
      this.instance = MqttConnect(parseConnection(this.config.get('mqtt.uri')));
      this.instance.once('connect', () => {
        this.logger.info('Mosquitto initialized');
        resolve();
      });
    })
  }

  async disconnect(): Promise<void> {
  }
}
