import { injectable, inject } from 'inversify';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../logger';
import {IMqttManager, IMQTTMANAGER} from './manager';

export type TMessageHandler<T = any> = (msg: T, topic: string) => void;

export interface IPubSub {
  sub<T = any>(topic: string, cb: TMessageHandler<T>): IPubSub;
  pub<T = any>(topic: string, msg: T): IPubSub;
}

export const IPUBSUB = Symbol.for('IPubSub');

const mqtt_regex = require('mqtt-regex');

@injectable()
export class PubSub implements IPubSub {
  private subscriptions = new Set<string>();
  private logger: ILogger;

  constructor(
    @inject(ILOGGERFACTORY) factory: ILoggerFactory,
    @inject(IMQTTMANAGER) private manager: IMqttManager,
  ) {
    this.logger = factory('pubsub');
  }

  sub<T = any>(topic: string, cb: TMessageHandler<T>): IPubSub {
    this.logger.log(`Subscribe ${topic}`);
    const subs = this.subscriptions;
    if (!subs.has(topic)) {
      this.manager.client.subscribe(topic);
      subs.add(topic);
    }

    let regex = mqtt_regex(topic);

    this.manager.client
      .on('message', (t, m) => {
        if (regex.exec(t)) {
          setImmediate(() => cb(JSON.parse(m.toString()), t));
        }
      });
    return this;
  }

  pub<T = any>(topic: string, msg: T): IPubSub {
    let data = JSON.stringify(msg);
    this.logger.log(`Publish ${topic} ${data}`);
    this.manager.client.publish(topic, data);
    return this;
  }
}
