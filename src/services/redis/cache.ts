import { injectable, inject } from 'inversify';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../logger';
import {IRedisManager, IREDISMANAGER} from './manager';

export interface ICache {
  getAll<T = any>(tag: string, def?: T): Promise<Record<string, T>>;
  get<T = any>(tag: string, key: string, def?: T): Promise<T>;
  set<T = any>(tag: string, key: string, value: T): Promise<T>;
}

export const ICACHE = Symbol.for('ICache');

@injectable()
export class Cache implements ICache {
  private logger: ILogger;

  constructor(
    @inject(ILOGGERFACTORY) factory: ILoggerFactory,
    @inject(IREDISMANAGER) private manager: IRedisManager,
  ) {
    this.logger = factory('cache');
  }

  async get<T = any>(tag: string, key: string, def: T = null): Promise<T> {
    this.logger.log(`Get ${tag}::${key}`);
    const value = await this.manager.client.hget(tag, key);
    if (!value) {
      return def;
    }
    return JSON.parse(value as string) || def;
  }

  async getAll<T = any>(tag: string): Promise<Record<string, T>> {
    this.logger.log(`Get ${tag}::*`);
    const values: Record<string, string> = await this.manager.client.hgetall(tag);
    Object.keys(values).forEach(key => values[key] = JSON.parse(values[key]));
    return values as any;
  }

  async set<T = any>(tag: string, key: string, value: T): Promise<T> {
    this.logger.log(`Set ${tag}::${key}`);
    await this.manager.client.hset(tag, key, JSON.stringify(value));
    return value;
  }
}
