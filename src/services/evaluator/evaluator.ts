import {inject, injectable} from 'inversify';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../logger';
import {IStorage, ISTORAGE, ITimestamped} from '../elastic';
import {ICache, ICACHE} from '../redis';
import {IPubSub, IPUBSUB} from '../mqtt';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import {AttributeModel, DeviceModel} from '../../models';

const _eval = require('node-eval');

export interface IEvaluator {
  eval<T = void>(device: string, lambda: string, value: any): Promise<T>;
}

export const IEVALUATOR = Symbol.for('IEvaluator');

interface IDeviceRuntime {
  get<T = any>(key: string): Promise<T | null>;
  set<T = any>(key: string, value: T): Promise<T>;
  // getSetting<T = any>(key: string): Promise<T>;
  // setSetting<T = any>(key: string, value: T): Promise<void>;
  query<T = any>(key: string | undefined, since: number | Date, till?: number | Date, interval?: string): Promise<ITimestamped[]>;
  pub<T = any>(topic: string, value: T): void;
}

function getDate(date: number | Date | undefined): Date {
  if (typeof date === 'number') {
    return new Date(new Date().getTime() + date * 1000);
  }
  if (date instanceof Date) {
    return date;
  }
  return new Date();
}

@injectable()
export class Evaluator implements IEvaluator {
  private logger: ILogger;

  @inject(ICACHE)
  private cache!: ICache;

  @inject(ISTORAGE)
  private storage!: IStorage;

  @inject(IPUBSUB)
  private pubsub!: IPubSub;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService!: IEntityService<AttributeModel>;

  constructor(
    @inject(ILOGGERFACTORY) factory: ILoggerFactory,
  ) {
    this.logger = factory('evaluator');
  }

  public async eval<X = any>(tag: string, lambda: string, msg: any): Promise<X> {
    this.logger.log('LAMBDA: ', lambda);

    const runtime: IDeviceRuntime = {
      get: <T = any>(key: string): Promise<T | null> =>
        this.cache.get(tag, key, null),

      set: <T = any>(key: string, value: T): Promise<T> =>
        this.cache.set(tag, key, value),

      query: <T = any>(
        key: string | undefined,
        since: number | Date,
        till?: number | Date,
        interval?: string,
      ): Promise<ITimestamped[]> =>
        this.storage.query(tag, [key], getDate(since), getDate(till), interval || '1m'),

      pub: <T = any>(topic: string, value: T): void =>
        this.pubsub.pub(`${tag}/${topic}`, value) && null,
    };

    let r = _eval(`module.exports = ((${lambda})(runtime, value));`, `/lambdas/${tag}`, { runtime, value: msg});
    this.logger.log('result: ', r);
    if (r instanceof Promise) {
      this.logger.log('await: ', r);
      r = await (r as Promise<any>);
      this.logger.log('awaited: ', r);
    }

    return r;
  }
}
