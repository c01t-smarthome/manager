import {injectable, inject, Container} from 'inversify';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from './services/logger';
import {setInterval} from 'timers';
import {ICache, ICACHE, IRedisManager} from './services/redis';
import {IMqttManager, IPubSub, IPUBSUB} from './services/mqtt';
import {setUpHandlerContainer} from './ioc-container';
import {IMessageHandler} from './handlers';
import {ITickHandler} from './handlers';
import {IElasticManager} from './services/elastic';

export interface IDeviceManager {
  start(): Promise<void>;
  stop(): Promise<void>;
}

@injectable()
export class DeviceManager implements IDeviceManager {
  private logger: ILogger;
  private lastScheduleTick = 0;
  private timer: NodeJS.Timer;

  @inject(Symbol.for('Container'))
  private kernel!: Container;

  @inject(ICACHE)
  private cache!: ICache;

  @inject(IPUBSUB)
  private pubsub!: IPubSub;

  @inject(Symbol.for('IRedisManager'))
  private redisManager!: IRedisManager;

  @inject(Symbol.for('IMqttManager'))
  private mqttManager!: IMqttManager;

  @inject(Symbol.for('IElasticManager'))
  private elasticManager!: IElasticManager;

  constructor(
    @inject(ILOGGERFACTORY) factory: ILoggerFactory,
  ) {
    this.logger = factory('worker');
  }

  public async stop(): Promise<void> {

  }

  public async start(): Promise<void> {
    await Promise.all([
      this.redisManager.connect(),
      this.mqttManager.connect(),
      this.elasticManager.connect(),
    ]);

    this.logger.log('Starting manager');

    this.lastScheduleTick = await this.cache.get('scheduler', 'last-tick', new Date().getTime() - 4 * 3600000);

    this.pubsub.sub('+/manifest', this.onManifest);
    this.pubsub.sub('+/reported/+', this.onReported);
    this.pubsub.sub('+/desired/+', this.onDesired);
    this.pubsub.sub('+/updated/+', this.onUpdated);

    this.timer = setInterval(this.onTick, 15000);
    await this.onTick();
  }

  private onManifest = (msg: any, topic: string) => {
    const container = this.kernel.createChild();
    setUpHandlerContainer(container);
    return container.get<IMessageHandler>(Symbol.for('ManifestHandler')).handle(msg, topic);
  };

  private onReported = (msg: any, topic: string) => {
    const container = this.kernel.createChild();
    setUpHandlerContainer(container);
    return container.get<IMessageHandler>(Symbol.for('ReportedHandler')).handle(msg, topic);
  };

  private onDesired = (msg: any, topic: string) => {
    const container = this.kernel.createChild();
    setUpHandlerContainer(container);
    return container.get<IMessageHandler>(Symbol.for('DesiredHandler')).handle(msg, topic);
  };

  private onUpdated = (msg: any, topic: string) => {
    const container = this.kernel.createChild();
    setUpHandlerContainer(container);
    return container.get<IMessageHandler>(Symbol.for('UpdatedHandler')).handle(msg, topic);
  };

  private onTick = async () => {
    const container = this.kernel.createChild();
    setUpHandlerContainer(container);

    const thisScheduleTick = new Date().getTime() - 1;
    await container.get<ITickHandler>(Symbol.for('TickHandler')).handle(thisScheduleTick, this.lastScheduleTick);

    this.lastScheduleTick = await this.cache.set('scheduler', 'last-tick', thisScheduleTick);
  }
}
